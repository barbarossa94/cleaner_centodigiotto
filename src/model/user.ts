export class User {

    constructor(public id: number, public name: string, public surname: string, public week: string, public evenDay: string, public oddDay: string, public telegramId?: number) { }

}