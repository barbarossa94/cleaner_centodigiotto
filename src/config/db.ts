import { User } from './../model/user';


export class DatabaseUsers{

    private database = new Array<User>();

    constructor(){
        this.database.push(new User(1,'Gianni','Filannino', 'E','C','B',156340013));
        this.database.push(new User(2,'Antonio','Di Lallo', 'E', 'B', 'C',432883825));
        this.database.push(new User(3,'Valerio','Tanferna', 'O','C','B',213316483));
        this.database.push(new User(4,'Utente','Utente', 'O','B','C'));
    }

    getById(id: number) : User{
        return this.database.filter(e => e.id === id)[0];
    }

    getByName(name: string): User{
        return this.database.filter(e => e.name === name)[0];
    }

    getWeekEvenUser() : User[]{
        return this.database.filter(e => e.week === 'E');
    }

    getWeekOddUser() : User[]{
        return this.database.filter(e => e.week === 'O');
    }

}