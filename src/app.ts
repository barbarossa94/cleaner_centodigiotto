import { User } from './model/user';
import moment from 'moment';
import { DatabaseUsers } from './config/db';
import { TelegramManager } from './telegram';
import { messages } from './messages/message'
import * as schedule from 'node-schedule';

export class App {

    private gianni: User;
    private antonio: User;
    private valerio: User;
    private database = new DatabaseUsers();
    private telgramManager = new TelegramManager();

    constructor() {
        this.gianni = this.database.getByName('Gianni');
        this.antonio = this.database.getByName('Antonio');
        this.valerio = this.database.getByName('Valerio');
    }

    checkTurn() {
        const week = parseInt(moment().format('w'));
        const dayOfMonth = parseInt(moment().format('dd'));
        const dayOfWeek = parseInt(moment().format('d'));

        if (6 == dayOfWeek || 0 == dayOfWeek) {
            if (week % 2 == 0) {
                let users = this.database.getWeekEvenUser();
                users.forEach(u => {
                    if (dayOfMonth % 2 == 0) {
                        if (u.evenDay == 'C') {
                            this.telgramManager.sendMessage(u, messages.dayMessage.cucina);
                        } else {
                            this.telgramManager.sendMessage(u, messages.dayMessage.bagno);
                        }
                    } else {
                        if (u.oddDay == 'C') {
                            this.telgramManager.sendMessage(u, messages.dayMessage.cucina);
                        } else {
                            this.telgramManager.sendMessage(u, messages.dayMessage.bagno);
                        }
                    }

                });
            } else {
                let users = this.database.getWeekOddUser();
                users.forEach(u => {
                    if (dayOfMonth % 2 == 0) {
                        if (u.evenDay == 'C') {
                            this.telgramManager.sendMessage(u, messages.dayMessage.cucina);
                        } else {
                            this.telgramManager.sendMessage(u, messages.dayMessage.bagno);
                        }
                    } else {
                        if (u.oddDay == 'C') {
                            this.telgramManager.sendMessage(u, messages.dayMessage.cucina);
                        } else {
                            this.telgramManager.sendMessage(u, messages.dayMessage.bagno);
                        }
                    }

                });
            }
        }
    }

    sendMessageTest(){
        this.telgramManager.sendMessage(this.gianni, 'Test by Gianni. Ciao GIANNI!');
    }

}

const app = new App();
const rule = new schedule.RecurrenceRule();
rule.hour = 7;
rule.minute = 0;

const ruleTest = new schedule.RecurrenceRule();
ruleTest.second = 10;

schedule.scheduleJob(rule, () => {
    app.checkTurn();
});

schedule.scheduleJob(ruleTest, () => {
    app.sendMessageTest();
});


